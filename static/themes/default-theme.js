(() => {
    const stylee = document.createElement('link');
    stylee.rel = 'stylesheet';
    stylee.setAttribute('id', 'themeStyle')

    if (localStorage.theme) {
        stylee.href = '/static/themes/' + localStorage.theme + '.css';
        document.head.appendChild(stylee);
    } else {
        const oReq = new XMLHttpRequest();
        oReq.onreadystatechange = function () {
            if (oReq.readyState == 4 && oReq.status == 200) {
                let theme = JSON.parse(oReq.responseText).theme.default || 'dark';
                stylee.href = '/static/themes/' + theme + '.css';
                document.head.appendChild(stylee);
            }
        }
        oReq.open('GET', '/static/json/config.json', false);
        oReq.send();
    }
})()