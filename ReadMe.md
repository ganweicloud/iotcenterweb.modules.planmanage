# 使用说明

 ## 安装依赖

 注意：因为使用插件原因，node.js下载对应版本安装，

[v10.14.1 x86](https://nodejs.org/download/release/v10.14.1/node-v10.14.1-x86.msi),
[v10.14.1 x64](https://nodejs.org/download/release/v10.14.1/node-v10.14.1-x64.msi)

安装之后请按照安装路径部署环境变量，示例：https://blog.csdn.net/lk1114385092/article/details/122374997

1. 在git base Here (或者VS CODE终端)中输入以下命令，更改npm指向:  
   `npm config set registry http://139.9.43.67:8081/repository/npm-group/`

2. 以上指向是私有库路径，需要输入npm login进行登录
   - 账号：**ganweisoft**
   - 密码：**ganwei.123** 
   - 邮箱：**ganweisoft@ganweisoft.com**

   通过以上操作，可以通过指令npm config ls，查看基本的信息。

3. 在 “项目” 根目录下使用 `npm install` 指令安装依赖，安装成功后，在项目文件夹中会多一个`node_modules`文件夹 。
    如果 `npm install` 安装报错，请根据提示百度解决，或者可以参考[安装依赖包问题](#安装依赖包问题)

    ##### 注意，可以尝试分开安装，查看是哪块因素导致，因为group是整合了私有服务器以及官网下载。比如先在package.json的 dependencies删除gw-base-*开头的文件，然后通过npm run public把安装路径指向官网，执行npm install安装完整后；再恢复删除的gw-base-*文件，再转向路径npm run private，继续执行npm install.
4. 打开根目录下 `config/index.js` 文件，修改项如下：

   - 变量 `hostIP` 值为本地IP+PORT,本地示例为： `https://192.168.XXX.XXX:44380` ; 
 
 ## 打包部署

 1. 业务场景需要，需要独立打包各个模块，采用了多页打包方式。使用 `npm run build-all`即可打包插件。其中，可以单独打包个别模块，修改配置如下图，在`config/dev.env.js`中，默认是配置在`views/pages`目录下（这里的配置引用涉及到`webpack`，请于 `pages` 下开发，没必要不用修改）：  
    ![打包配置](./media/buildConfig.png)

 2. 打包后编译生成的前端包，放在根目录下的dist目录中(生成的ganwei-iotcenter-login和static两个包可以忽略)。打包结果如图：
    ![打包结果](./media/bundles.png)

 3. 把生成的模块直接拷贝到wwwroot文件夹下。如下图所示：  
    ![打包结果](./media/wwwroot.png)

 4. 因没有后端插件生成菜单，所以直接在GwMenu表中插入菜单数据，数据库脚本如下：
   ```
   INSERT INTO `gwmenu` (`Id`, `Code`, `Name`, `ParentId`, `LinkUrls`, `Route`, `Level`, `Path`, `Icon`, `NodeType`, `Order`, `MenuOwner`, `Enabled`, `MenuName`, `PackageId`) VALUES ('1680', 'planManagement', 'menuJson.planManagement.title', '0', '1', '', '1', '', 'iconzichanqingdan', '1', '1', '0', '1', '预案管理', NULL);
   INSERT INTO `gwmenu` (`Id`, `Code`, `Name`, `ParentId`, `LinkUrls`, `Route`, `Level`, `Path`, `Icon`, `NodeType`, `Order`, `MenuOwner`, `Enabled`, `MenuName`, `PackageId`) VALUES ('1681', '', 'menuJson.planManagement.children[0].name', '1680', '1', '/Index/jumpIframe/ganwei-base-plan-management/planManagement', '2', '', 'iconzichanqingdan', '2', '2', '0', '1', '预案管理', NULL);
   ```
   GWMenu数据库字段含义如下（ID不能重复）:  
    ![GWMenu字段](./media/gwmenu.png)
    另:
    Enabled --- 是否启用菜单
    MenuName --- 菜单名称
    PackageId --- 包ID，通过后端插件生成菜单时此列复制，此处可为空
    Route --- 路由的配置为 **/Index/jumpIframe/ganwei-base-plan-management/planManagement**。其中 **/Index/jumpIframe/** 为固定部分 **ganwei-base-plan-management** 为src/views/pages下的模块名称。**/planManagement** 为src/views/pages/ganwei-base-plan-management/router.js文件中的路由，如下图所示
    ![路由](./media/rote.png)
 5. 开启服务，登录IoT平台后可以看到预案管理的菜单和页面，如下图所示：
    ![plan](./media/plan.png)

 ## 文件说明
  
 1. 目录说明  
   ![目录说明](./media/ganweiProject.png)

 2. ganwei-base-plan-management模块项目文件在`src/views/pages/ganwei-base-plan-management`目录下，其目录结构如下:  
   ![ganwei-base-plan-management目录](./media/module.png)

 3. `src/request/api/PlanManage.js`是API结构封装，新增API可按照格式填写即可,如图所示：
    ![API](./media/templateApi.png)  
    以上引用会在`src/request/api.js`中集成  
    ![API集成](./media/api.png)  
    相关请求方法已封装在`node_module/gw-base-api/apiFunction`中  
    ![请求方法](./media/apiFunction.png)

 4. 前端插件详细开发说明请参照[开发者文档-第四章 开发指南-应用插件开发-前端插件开发](https://www.ganweicloud.com/docs/intro/3%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97/3.5%E5%BA%94%E7%94%A8%E6%8F%92%E4%BB%B6%E5%BC%80%E5%8F%91/3.5.1%E5%89%8D%E7%AB%AF%E6%8F%92%E4%BB%B6%E5%BC%80%E5%8F%91/)
 #### 安装依赖包问题
1. 安装依赖包时报错无法找到python2.x路径  
    下载[python2.7版本](https://www.python.org/ftp/python/2.7/python-2.7.msi)并安装

2. 安装依赖包时出现`MSB4132`错误  
    `MSBUILD : error MSB4132: The tools version "2.0" is unrecognized. Available tools versions are "4.0".`

    打开命令行，如果执行了指向敢为私有库，请重新更改指向，执行  
   - `npm config set registry https://registry.npmjs.org `
   - `npm install --global --production windows-build-tools`
   - `npm config set msvs_version 2015 --global`

    最后进入项目所在文件夹，更改指向，执行  
   - `npm config set registry http://139.9.43.67:8081/repository/npm-group/`  
   - `npm install`

   [参考链接](https://www.cnblogs.com/mmoonan/archive/2017/04/06/6674422.html)
