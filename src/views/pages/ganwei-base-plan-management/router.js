/*************************************************************************************
 * Copyright (C) 2022 深圳市敢为软件技术有限公司 版权所有。
 * 文 件 名:   router.js
 * 描    述:    
 * 版    本：  V1.0
 * 创 建 者：  沈东
 * 创建时间：  2021-5-20
 * 修 改 者：  沈东
 * 修改时间：  2021-5-21
 * ======================================
*************************************************************************************/
/* eslint-disable */
import Vue from 'vue'
import Router from 'vue-router'
import planManagement from './src/planManagement'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/planManagement',
            name: 'planManagement',
            component: planManagement
        }
    ]
})