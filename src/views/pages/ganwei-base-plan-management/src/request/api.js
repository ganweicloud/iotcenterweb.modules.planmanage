/**
 * api模块接口列表
 */
import apiFunction from 'gw-base-api/apiFunction';
import PlanManage from './api/PlanManage';

const api = Object.assign(
    {},
    apiFunction,
    PlanManage
);

export default api;
