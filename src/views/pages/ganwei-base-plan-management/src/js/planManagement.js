import privacyStatement from '../components/privacyStatement/privacyStatement.vue';

export default {
    components: {
        privacyStatement
    },
    data() {
        const checkPlanName = (rule, value, callback) => {
            const key = /^[\w\u4e00-\u9fa5]+$/;
            if (!value.match(key)) {
                return callback(new Error(this.$t('planManagement.tips.only')));
            }
            callback();
        };
        return {
            planList: [], // 预案列表
            tableHeight: 736,

            // 请求头
            planHeader: {
                Authorization: window.sessionStorage.getItem('accessToken')
            },

            // x-xsrf-token
            headers: {
                'x-xsrf-token': document.cookie.split('=')[1]
            },
            loading: false,
            planDialog: false, // 添加、编辑预案dialog开关
            delPlan: false, // 删除预案提示框开关

            // 添加、编辑dialog内容
            planForm: {
                planId: 0, // 预案ID
                planName: '', // 预案名
                planWord: '', // 预案内容
                planFiles: [] // 预案附件
            },

            editAddTitle: this.$t('planManagement.plan.add'),
            editReadonly: false, // 编辑内容是否为只读模式
            uploadFilesUrl: '', // 预案附件上传接口地址
            autoUpload: false, // 是否自动上传附件

            pageSize: 25, // 每页请求数据量
            pageNo: 1, // 当前页码
            total: -1, // 数据总量

            srcList: [],

            reqUrl: '', // 当前服务器地址
            fileType: [
                'image/pcx',
                'image/jpeg',
                'image/gif',
                'image/tiff',
                'image/psd',
                'image/png',
                'image/sgi',
                'image/jpg',
                'image/ras'
            ],
            editAddRules: {
                planName: [
                    { required: true, message: this.$t('planManagement.tips.enterName'), trigger: 'blur' },
                    { validator: checkPlanName, trigger: 'blur' }
                ],
                planWord: [{ required: true, message: this.$t('planManagement.tips.enterContent'), trigger: 'blur' }]
            },
            searchPlanName: '' // 搜索预案名
        };
    },

    mounted() {
        let urls = window.location.href;
        urls = urls.split('?');
        window.location.href = urls[0];
        this.reqUrl = this.$api.getSignalrHttp();
        this.uploadFilesUrl = this.reqUrl + '/IoT/api/v3/ReservePlan/UploadAttach';
        let that = this;
        let box = document.getElementById('elTable');
        window.onresize = function windowResize() {
            box = document.getElementById('elTable');
            if (box) {
                that.tableHeight = box.offsetHeight;
            }
        };
        that.getPlanList();
    },
    updated() {
        let box = document.getElementById('elTable');
        if (box) {
            this.tableHeight = box.offsetHeight;
        }
    },

    methods: {

        // 获取预案数据列表
        getPlanList() {
            this.loading = true;
            let data = {
                pageNo: this.pageNo,
                pageSize: this.pageSize,
                planName: this.searchPlanName
            };
            this.$api.getPlanList(data).then((res) => {
                this.loading = false;
                if (res.data.code == 200) {
                    let planList = res.data.data.rows;
                    let dataList = [];
                    let obj = {};
                    if (planList.length >= 0) {
                        planList.forEach((item, i) => {
                            obj = {
                                planId: item.planId, // 预案ID
                                planName: item.planName, // 预案名
                                createTime: item.createTime, // 创建时间
                                planCreater: item.planCreater, // 创建人
                                revisionTime: item.revisionTime, // 修改时间
                                reviser: item.reviser, // 修改人
                                planStatus: item.planStatus === 0 ? this.$t('planManagement.model.draft') : this.$t('planManagement.table.pulished'), // 预案状态
                                planWord: decodeURIComponent(item.planWord), // 预案内容
                                planFiles: item.attachmentList, // 预案附件
                                isBandEquip: item.isBandEquip // 预案是否被设备或设备测点使用
                            }
                            dataList.push(obj);
                        });
                        this.planList = dataList;
                    }
                    this.total = res.data.data.totalCount;
                } else {
                    this.$message.error(this.$t('planManagement.tips.failedToQuery'));
                }
            }).catch(err => {
                this.$message.error(err.data.description);
                this.loading = false;
            });
        },

        // 搜索预案管理
        inputChange(params) {
            this.searchPlanName = params;
            this.pageNo = 1;
            this.getPlanList();
        },

        // 按钮操作弹窗
        onFrame(no, item) {
            if (no === 0) { // 删除该项预案
                this.delPlan = true;
            }
            if (no === 1) {
                this.editAddTitle = this.$t('planManagement.plan.add');
                this.editReadonly = false;
                this.planForm = {
                    planId: 0,
                    planName: '',
                    planWord: '',
                    planFiles: []
                };
                this.planDialog = true;
                this.autoUpload = false;
            }
            if (no === 2) {
                this.editAddTitle = this.$t('planManagement.plan.edit');
                this.editReadonly = false;
                this.planDialog = true;
                this.autoUpload = true;
            }
            if (no === 3) {
                this.editAddTitle = this.$t('planManagement.plan.view');
                this.editReadonly = true;
                this.planDialog = true;
            }
        },

        // 资产图片预览
        onSeeImg(imgUrl) {
            this.srcList = [];
            this.srcList[0] = this.reqUrl + imgUrl;
        },

        // 编辑预案数据---弹窗
        getTableData(item, column, event) {

            // 删除预案
            if (this.delPlan) {
                this.$confirm(this.$t('planManagement.tips.isDelete'), this.$t('planManagement.tips.tips'), {
                    confirmButtonText: this.$t('publics.button.confirm'),
                    cancelButtonText: this.$t('publics.button.cancel'),
                    type: 'warning'
                }).then(() => {
                    if (item.isBandEquip) {
                        this.$confirm(this.$t('planManagement.tips.runningAndDelete'), this.$t('planManagement.tips.tips'), {
                            confirmButtonText: this.$t('publics.button.confirm'),
                            cancelButtonText: this.$t('publics.button.cancel'),
                            type: 'warning'
                        }).then(res => {
                            this.$api.deletePlan(item.planId).then(res => {
                                this.delPlan = false;
                                if (res.data.code == 200) {
                                    this.$message({
                                        message: this.$t('planManagement.tips.deletedSucc'),
                                        type: 'success'
                                    });
                                    this.getPlanList();
                                } else {
                                    this.$message.error(res.data.message);
                                }
                            });
                        }).catch(() => {
                            this.$message({
                                type: 'info',
                                message: this.$t('planManagement.tips.deletedCanceled')
                            });
                            this.delPlan = false;
                        });
                    } else {
                        this.$api.deletePlan(item.planId).then(res => {
                            this.delPlan = false;
                            if (res.data.code == 200) {
                                this.$message({
                                    message: this.$t('planManagement.tips.deletedSucc'),
                                    type: 'success'
                                });
                                this.getPlanList();
                            } else {
                                this.$message.error(res.data.message);
                            }
                        });
                    }
                }).catch(() => {
                    this.$message({
                        type: 'info',
                        message: this.$t('planManagement.tips.deletedCanceled')
                    });
                    this.delPlan = false;
                });
            }

            this.planForm = {
                planId: item.planId,
                planName: item.planName,
                planWord: item.planWord,
                planFiles: item.planFiles
            };
        },

        // 添加、编辑保存 预案数据
        createPlan(planStatus) {
            this.$refs['planForm'].validate((valid) => {
                if (valid) {
                    if (this.editAddTitle === this.$t('planManagement.plan.add')) {
                        let formData = new FormData();
                        formData.append('planName', this.planForm.planName);
                        formData.append('planStatus', planStatus);
                        formData.append('planWord', encodeURIComponent(this.planForm.planWord));
                        for (let file of this.planForm.planFiles) {
                            formData.append('files', file.raw); // 用于传复数文件
                        }
                        this.$api
                            .createPlan(formData)
                            .then((res) => {
                                if (res.data.code == 200) {
                                    this.$message({
                                        message: this.$t('planManagement.tips.addedSucc'),
                                        type: 'success'
                                    });
                                    this.pageNo = 1;
                                    this.planDialog = false;
                                    this.getPlanList();
                                    this.$refs['planForm'].resetFields();
                                } else {
                                    this.$message.error(res.data.message);
                                }
                            })
                            .catch((err) => {
                                this.$message.error(err.data.description);
                            });
                    } else {

                        // 编辑预案保存
                        let data = {
                            planId: this.planForm.planId,
                            planName: this.planForm.planName,
                            planStatus: planStatus,
                            planWord: encodeURIComponent(this.planForm.planWord)
                        };
                        this.$api.editPlan(data).then((res) => {
                            if (res.data.code == 200) {
                                this.$message({
                                    message: this.$t('planManagement.tips.changedSucc'),
                                    type: 'success'
                                });
                                this.planDialog = false;
                                this.$refs['planForm'].resetFields();
                                this.getPlanList();
                            } else {
                                this.$message.error(res.data.message);
                            }
                        }).catch(err => {
                            this.$message.error(err.data.description);
                        })
                    }
                }
            });
        },

        // 关闭dialog
        onCloseCurve() {
            this.planDialog = false;
            this.$refs['planForm'].resetFields();
            this.planForm.planFiles = []; // 清空附件数组
        },

        // 上传前验证
        beforeAvatarUpload(file) {
            let isJPG = false;
            if (file.type && this.fileType.indexOf(file.type) > -1) {
                isJPG = true;
            }
            const isLt2M = file.size / 1024 / 1024 < 2;

            if (!isJPG) {
                this.$message.error(this.$t('planManagement.tips.formatError'));
            }
            if (!isLt2M) {
                this.$message.error(this.$t('planManagement.tips.sizeExceed'));
            }
            return isJPG && isLt2M;
        },

        // 页码改变钩子
        handleCurrentChange(val) {
            this.pageNo = val;
            this.getPlanList();
        },

        // 页数据量改变钩子
        handleSizeChange(val) {
            this.pageSize = val;
            this.getPlanList();
        },

        // 上传成功的钩子（接口状态码为200）
        uploadSuccess(res, file, fileList) {
            if (res.code === 200) {
                this.$message({
                    message: this.$t('planManagement.tips.uploadSucc'),
                    type: 'success'
                });
                this.getPlanList(); // 上传成功后刷新表格获取最新数据
            } else {
                this.$message({
                    message: this.$t('planManagement.tips.unknown'),
                    type: 'info'
                });
            }
        },

        // 上传失败的钩子
        uploadError(err, file, fileList) {
            if (err) {
                this.$message({
                    message: this.$t('planManagement.tips.uploadFailed'),
                    type: 'error'
                });
            }
        },

        // 删除附件前
        beforeRemove(file, fileList) {
            return this.$confirm(this.$t('planManagement.tips.isRemove'), this.$t('planManagement.tips.tips'), {
                confirmButtonText: this.$t('publics.button.confirm'),
                cancelButtonText: this.$t('publics.button.cancel'),
                type: 'warning'
            }).then(() => {
                this.$api.deletePlanFile(file.id).then(res => {
                    if (res.data.code === 200) {
                        this.$message({
                            message: this.$t('planManagement.tips.removeSucc'),
                            type: 'success'
                        });
                        this.getPlanList(); // 移除成功后刷新表格获取最新数据
                    }
                })
                    .catch(() => {
                        this.$message({
                            message: this.$t('planManagement.tips.removeFail'),
                            type: 'error'
                        });
                    })
            })
                .catch(() => {
                    this.$message({
                        type: 'info',
                        message: this.$t('planManagement.tips.removeCanceled')
                    });
                });
        },

        // 文件状态改变钩子
        handleChange(file, fileList) {
            this.$set(this.planForm, 'planFiles', fileList); // 选中文件存入数组
        },

        // 下载文件
        downloadPlanFile(fileId, fileName) {
            const a = document.createElement('a'); // 创建a标签
            a.style.display = 'none';
            a.href = `${this.$api.getSignalrHttp()}/IoT/api/v3/ReservePlan/getDownLoadAttach?attachmenId=${fileId}`;
            a.download = fileName; // 重命名文件
            a.click();
            a.remove();
        }
    }
};
