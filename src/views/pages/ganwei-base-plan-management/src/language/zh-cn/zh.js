import publics from 'gw-base-language/zh-cn/publics.js';
import zhLocale from 'element-ui/lib/locale/lang/zh-CN'
import planManagement from './planManagement.js'
export default {
    publics,
    ...zhLocale,
    planManagement
}
