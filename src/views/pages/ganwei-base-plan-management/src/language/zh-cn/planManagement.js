module.exports = {
    title: '预案管理',
    search: '搜索预案名称',
    plan: {
        add: '添加预案',
        no: '暂无预案',
        edit: '编辑预案',
        view: '查看预案'
    },
    table: {
        planNo: '预案号',
        planName: '预案名称',
        creationTime: '创建时间',
        founder: '创建人',
        revisionTime: '修改时间',
        lastReviser: '最后修改人',
        planStatus: '预案状态',
        operation: '操作'
    },
    model: {
        planName: '预案名称',
        content: '预案内容',
        appendix: '预案附件',
        upload: '附件上传',
        download: '下载',
        noAttachment: '暂无附件',
        cancel: '取消',
        release: '发布',
        saveDraft: '存草稿',
        draft: '草稿',
        pulished: '已发布'
    },
    tips: {
        only: '只能输入汉字、字母、数字、下划线',
        enterName: '请输入预案名称',
        enterContent: '请输入预案内容',
        failedToQuery: '查询预案列表失败',
        isDelete: '是否删除该预案？',
        tips: '提示',
        runningAndDelete: '该预案正在被设备使用，确定删除该预案？',
        deletedSucc: '删除成功！',
        deletedCanceled: '已取消删除',
        addedSucc: '添加成功！',
        changedSucc: '更改成功！',
        formatError: '上传头像图片格式错误！',
        sizeExceed: '上传头像图片大小不能超过2MB！',
        uploadSucc: '上传文件成功！',
        unknown: '出现未知问题，请联系相关人员解决！',
        uploadFailed: '上传文件失败！',
        isRemove: '确定移除该附件？',
        removeSucc: '移除成功！',
        removeFail: '移除失败！',
        removeCanceled: '已取消移除'
    }
}