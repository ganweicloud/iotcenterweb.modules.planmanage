import publics from 'gw-base-language/en-us/publics.js';
import enLocale from 'element-ui/lib/locale/lang/en'
import planManagement from './planManagement.js'

export default {
    publics,
    ...enLocale,
    planManagement
}
