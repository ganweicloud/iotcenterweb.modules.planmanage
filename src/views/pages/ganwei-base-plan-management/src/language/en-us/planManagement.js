module.exports = {
    title: 'Plan Management',
    search: 'Search plan name',
    plan: {
        add: 'Add plan',
        no: 'No plan',
        edit: 'Edit plan',
        view: 'View plan'
    },
    table: {
        planNo: 'Plan No',
        planName: 'Plan name',
        creationTime: 'Creation time',
        founder: 'Founder',
        revisionTime: 'Revision time',
        lastReviser: 'Last reviser',
        planStatus: 'Plan status',
        operation: 'Operation'
    },
    model: {
        planName: 'Plan name',
        content: 'Contents',
        appendix: 'Appendix',
        upload: 'upload',
        download: 'download',
        noAttachment: 'No attachment',
        cancel: 'Cancel',
        release: 'Release',
        saveDraft: 'Save Draft',
        draft: 'draft',
        pulished: 'pulished'
    },
    tips: {
        only: 'Only Chinese characters, letters, numbers and underscores can be entered',
        enterName: 'Please enter the name of the plan',
        enterContent: 'Please input the content of the plan',
        failedToQuery: 'Failed to query plan list',
        isDelete: 'Delete the plan?',
        tips: 'Tips',
        runningAndDelete: 'The plan is being used by the equipment. Are you sure you want to delete the plan?',
        deletedSucc: 'Delete successfully!',
        deletedCanceled: 'Deletion canceled',
        addedSucc: 'Successfully added!',
        changedSucc: 'Successfully changed!',
        formatError: 'Upload avatar image format error!',
        sizeExceed: 'Upload avatar image size cannot exceed 2MB!',
        uploadSucc: 'Upload file successfully!',
        unknown: 'In case of unknown problems, please contact relevant personnel to solve them!',
        uploadFailed: 'Upload file failed!',
        isRemove: 'Are you sure you want to remove the attachment?',
        removeSucc: 'Removed successfully!',
        removeFail: 'Remove failed!',
        removeCanceled: 'Removal canceled'
    }
}